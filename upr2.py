#!/usr/bin/env python3
import random

artic = ["the", "be", "who", "ami"]
suchn = ["dog","cat", "table", "fork","food"]
msg = 0 # счетчик, используем в циклах
while True:  # вводим число
    line = input("integer: ")
    if line: # если введено число
        try:
            i = int(line)
            if  1 <= i <= 10: # i должно быть в диапазоне от 1 до 10
                while msg < i: # выводим количетво строк, равное i
                    print(str(msg)," " + random.choice(artic)," "  + random.choice(suchn)) # выводим построчно слова и номера строк
                    msg+=1
            else:
                print("Вы не попали в диапазон от 1 до 10")
            break
        except ValueError as err:
            print(err)
    else:
        while msg < 5: # если ничего не введено, выводим 5 раз, и завершаем работу
            print(random.choice(artic) + random.choice(suchn))
            msg+=1
        break
