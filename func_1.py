#!/usr/bin/env python3

def get_int(msg): # Объявляем фунцию принимающую число
    while True:
        try:
            i = int(input(msg))
            return i
        except ValueError as err: # если не число, то ошибка
            print(err)
age = get_int("Введите  число: ") # применяем функцию
print(age)
